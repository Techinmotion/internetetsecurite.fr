# How to Protect Against Cybercrime at Home #

With more people online than ever before, this has resulted in an increase in cybercrime. Whether it is people trying to steal your identity, scam you into handing over money, installing trojans on your computers, or trying to get their hands on your credit card information, there is so much to watch out for when online.

Use our tips below to ensure that your home is as protected as possible against cybercrime:


## Use a VPN ##

A home VPN is one of the most important pieces of software that you should use to enhance your online security. They will allow your devices and computers to hide their IP address, making it difficult for hackers to get access to them. The best VPN providers even allow multiple simultaneous connections, so they can protect each device in your home.

## Educate Your Family on Phishing Emails ##

Because of the fame of Nigerian princes promising to send vast amounts of money in return for a relatively small amount sent to them, you would think everyone could spot a phishing email these days. Unfortunately, that is not true. That scammers are still sending them shows that there are still people that fall for them. Make sure that your family is educated on this type of scam and that they know not to click links in emails, messengers, or on social media.

## Teach Your Children Not to Share Too Much on Social Media ##

We all know what kids are like, so it is little wonder that in their excitement they might share a little too much information on Facebook, Twitter, etc. Identity theft is real and children are just as much a target as adults. Teach them the importance of keeping private and personal information, well, private. 


## Stay Safe ##

By using the above tips, your home network and your family should be in a much better position to protect against cybercrime. Of course, you should always be on your toes as hackers and scammers will always find novel ways to target their victims.

[https://internetetsecurite.fr](https://internetetsecurite.fr)